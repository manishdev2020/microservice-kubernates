package com.freeworld.microservicekubernates;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WelcomeController {

	@GetMapping("/")
	public String welcomePage(final Model model) {
		model.addAttribute("title", "Microservie Kubenrates Helm docker project");
		model.addAttribute("msg", "This is Main");
		return "welcome";
	}
}
