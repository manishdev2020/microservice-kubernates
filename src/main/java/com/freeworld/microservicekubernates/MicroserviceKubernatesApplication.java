package com.freeworld.microservicekubernates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceKubernatesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceKubernatesApplication.class, args);
	}

}
