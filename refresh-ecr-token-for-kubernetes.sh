#!/bin/bash

# prerequisite:
# - ec2 instance should attached proper iam role
# - awscli
# - kubectl

# Usage:
#
# define the following variales in your environment (root account)
# - ECR_ACCOUNT
# - ECR_REGION
# - SECRET_NAME
#
# $ cp <script-file> /etc/cron.hourly/refresh_ecr_token
# $ chmod +x /etc/cron.hourly/refresh_ecr_token

# define ecr related information
ECR_ACCOUNT="121212"
ECR_REGION="ap-south-1"
SECRET_NAME="my-registry-key"
DOCKER_REGISTRY="https://${ECR_ACCOUNT}.dkr.ecr.${ECR_REGION}.amazonaws.com"

refresh_token() {
  # get latest ecr login token via awscli
  TOKEN=$(aws ecr get-authorization-token                    \
            --output text                                    \
            --query 'authorizationData[].authorizationToken' | \
          base64 -d | cut -d: -f2)

  # abort if token retrieval failed
  if [ -z "${TOKEN}" ]; then
    echo "==> Abort, get token failed"
    exit 1
  fi

  # remove previous created secret (any failure will be ignored)
  kubectl delete secret --ignore-not-found "${SECRET_NAME}" || true

  # refresh ecr token with new token
  kubectl create secret docker-registry "${SECRET_NAME}"     \
    --docker-server=${DOCKER_REGISTRY}                       \
    --docker-username=AWS                                    \
    --docker-password="${TOKEN}"
}

refresh_token "${ECR_REGION}"